unit UnitConnection;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, odbcconn, sqldb, FileUtil;

type

  { TDataModule1 }

  TDataModule1 = class(TDataModule)
    ODBCConnection1: TODBCConnection;
    SQLUsermaster: TSQLQuery;
    SQLTransaction1: TSQLTransaction;
    procedure ODBCConnection1AfterConnect(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  DataModule1: TDataModule1;

implementation

{$R *.lfm}

{ TDataModule1 }

procedure TDataModule1.ODBCConnection1AfterConnect(Sender: TObject);
begin
  //SQLUsermaster.Close;
  //SQLUsermaster.Open;
end;

end.

