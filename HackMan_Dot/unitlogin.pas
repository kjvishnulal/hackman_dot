unit UnitLogin;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls, EditBtn, Buttons, UnitConnection, sqldb,db;

type

  { TFLogin }

  TFLogin = class(TForm)
    BCon: TBevel;
    BBLogin: TBitBtn;
    BBClose: TBitBtn;
    EdUsername: TEdit;
    EdPassword: TEdit;
    Image1: TImage;
    LUser: TLabel;
    Lpass: TLabel;
    LCon: TLabel;
    procedure BBCloseClick(Sender: TObject);
    procedure BBLoginClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  FLogin: TFLogin;

implementation

{$R *.lfm}

{ TFLogin }

procedure TFLogin.FormShow(Sender: TObject);
begin
  Try
    if not DataModule1.ODBCConnection1.Connected then
       DataModule1.ODBCConnection1.Connected:= True;
  except
    ShowMessage('Database not connected ; check your connection');
    Application.Terminate;
    Exit;
  end;


  BCon.Visible:= False;
  LCon.Visible:= False;


  LUser.Visible:= True;
  Lpass.Visible:= True;
  EdUsername.Visible:= True;
  EdPassword.Visible:= True;
  BBLogin.Visible:= True;
  BBClose.Visible:= True;
end;

procedure TFLogin.BBLoginClick(Sender: TObject);
begin
  DataModule1.SQLUsermaster.Close;
  DataModule1.SQLUsermaster.Open;
  DataModule1.SQLUsermaster.First;
   if not DataModule1.SQLUsermaster.Locate('username',Trim(EdUsername.Text),[loCaseInsensitive]) then
   begin
      ShowMessage('User name found ');
      EdUsername.SetFocus;
      Exit;
   end;
   if Trim(EdPassword.Text) <> Trim(DataModule1.SQLUsermaster.fieldbyname('password').AsString) then
   begin
      ShowMessage('User name found ');
      EdUsername.SetFocus;
      Exit;
   end;
   ModalResult:= mrOK;
end;

procedure TFLogin.BBCloseClick(Sender: TObject);
begin
   application.Terminate;
end;

procedure TFLogin.FormCreate(Sender: TObject);
begin
//   self.ShowModal;
end;

end.

