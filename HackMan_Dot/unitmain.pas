unit UnitMain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, mysql50conn, mysql56conn, odbcconn, sqldb, db, FileUtil,
  DBDateTimePicker, DateTimePicker, Forms, Controls, Graphics, Dialogs, DBGrids,
  Buttons, Menus, DbCtrls, StdCtrls, ExtCtrls, DBExtCtrls, UnitConnection,
  UnitLogin;

type

  { TForm1 }

  TForm1 = class(TForm)
    Bevel1: TBevel;
    Bevel2: TBevel;
    Bevel3: TBevel;
    Bevel4: TBevel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    CBAssignee: TComboBox;
    ComboBox2: TComboBox;
    DateTimePicker1: TDateTimePicker;
    DBGComp: TDBGrid;
    DBMemo2: TDBMemo;
    DSComp: TDataSource;
    DBImage1: TDBImage;
    DBMemo1: TDBMemo;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    LCount: TLabel;
    Memo1: TMemo;
    Panel1: TPanel;
    PLogoff: TPanel;
    RGCat: TRadioGroup;
    SQLQComp: TSQLQuery;
    SQLQHistory: TSQLQuery;
    SQLQHistory1: TSQLQuery;
    SQLTransaction1: TSQLTransaction;
    procedure Bevel4ChangeBounds(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RGCatChangeBounds(Sender: TObject);
    procedure RGCatClick(Sender: TObject);
  private
    { private declarations }
    cCurrentUsername,cCurrentUserDept,cCurrentUserID : String;
    employeelist : TStringList;
    Procedure loadDate;
    procedure ClearControls;

  public
    { public declarations }

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.BitBtn1Click(Sender: TObject);
begin
   loadDate;
end;

procedure TForm1.BitBtn2Click(Sender: TObject);
var Assigneelist : TStringList;
begin
   Assigneelist := TStringList.Create;
   Assigneelist.Clear;
   Assigneelist.Delimiter:= '#';
   Assigneelist.DelimitedText:= trim(CBAssignee.Text);
   try
   SQLQHistory.ParamByName('Cid').AsInteger := SQLQComp.FieldByName('cid').AsInteger;
   SQLQHistory.ParamByName('Pdate').AsDateTime := DateTimePicker1.Date;
   SQLQHistory.ParamByName('Pstatus').AsString := ComboBox2.Text;
   SQLQHistory.ParamByName('Assignee').AsString := Assigneelist.Strings[0];
   SQLQHistory.ParamByName('Remarks').AsString := Memo1.Text;
   SQLQHistory.ParamByName('Username').AsString := cCurrentUserID;
   SQLQHistory.ExecSQL;

   SQLQHistory1.ParamByName('cstat').AsString:=ComboBox2.Text;;
   SQLQHistory1.ParamByName('cid').AsString:=SQLQComp.FieldByName('cid').AsString;
   SQLQHistory1.ExecSQL;
   finally
       Assigneelist.Free;
       SQLTransaction1.Rollback;
   end;
   loadDate;
   ClearControls;
   CBAssignee.SetFocus;
end;

procedure TForm1.BitBtn4Click(Sender: TObject);
begin
   Application.Terminate;
end;

procedure TForm1.FormActivate(Sender: TObject);
begin
   FLogin := TFLogin.Create(self);
   if not FLogin.ShowModal = mrOK then
      Application.Terminate;
   cCurrentUserDept:= DataModule1.SQLUsermaster.FieldByName('department').AsString;
   cCurrentUsername:= DataModule1.SQLUsermaster.FieldByName('Fullname').AsString;
   cCurrentUserID := DataModule1.SQLUsermaster.FieldByName('Uid').AsString;
   PLogoff.Visible:= False;
   employeelist.Clear;
   CBAssignee.Items.Clear;
   DataModule1.SQLUsermaster.First;
   while not DataModule1.SQLUsermaster.EOF do
   begin
      if cCurrentUserDept <> DataModule1.SQLUsermaster.FieldByName('department').AsString then
      begin
         DataModule1.SQLUsermaster.Next;
         Continue;
      end;
      employeelist.Add(DataModule1.SQLUsermaster.FieldByName('Uid').AsString+'#'+DataModule1.SQLUsermaster.FieldByName('Fullname').AsString);
      CBAssignee.Items.Add(DataModule1.SQLUsermaster.FieldByName('Uid').AsString+'#'+DataModule1.SQLUsermaster.FieldByName('Fullname').AsString);
      DataModule1.SQLUsermaster.Next;
   end;
   loadDate;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
   PLogoff.Align:= alClient;
   PLogoff.BringToFront;
   employeelist := TStringList.Create;
   employeelist.Clear;
   ClearControls;

end;

procedure TForm1.FormShow(Sender: TObject);
begin
end;

procedure TForm1.RGCatChangeBounds(Sender: TObject);
begin
end;

procedure TForm1.RGCatClick(Sender: TObject);
begin
   loadDate;
end;

procedure TForm1.loadDate;
begin
   if not DataModule1.ODBCConnection1.Connected then
   begin
      DataModule1.ODBCConnection1.Connected:= True;
      Exit;
   end;
   //if not SQLQComp.Active then
   //begin
   SQLQComp.SQL.Clear;
   SQLQComp.SQL.Add('SELECT * FROM comp c');
   SQLQComp.SQL.Add('INNER JOIN pusermaster pm on c.CMid = pm.Mid');
   SQLQComp.SQL.Add('INNER JOIN compdept cd on c.Cid = cd.DCid');
   SQLQComp.SQL.Add('WHERE cd.DeptID = :depid');
   if RGCat.ItemIndex <> 4 then
      SQLQComp.SQL.Add('AND c.CStatus = :status  ');

      SQLQComp.Close;
      SQLQComp.ParamByName('depid').AsString:= cCurrentUserDept;
   if RGCat.ItemIndex <> 4 then
      SQLQComp.ParamByName('status').AsString:= RGCat.Items.Strings[RGCat.ItemIndex];
      SQLQComp.Open;

      LCount.Caption:= 'Total Request Count : '+IntToStr(SQLQComp.RecordCount);
   //end;
end;

procedure TForm1.ClearControls;
begin
   CBAssignee.Text:= '';
   ComboBox2.Text:='';
   DateTimePicker1.Date:= Now;
   Memo1.Lines.Clear;
end;

procedure TForm1.Bevel4ChangeBounds(Sender: TObject);
begin
end;

end.

